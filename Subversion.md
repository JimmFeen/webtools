# SVN

## Change diff tool

### Create external script
```bash
#!/bin/sh
# Subversion provides the paths we need as the sixth and seventh
# parameters.
LEFT="$6"
RIGHT="$7"
# Call the diff command (change the following line to make sense for
# your merge program).
"C:\Program Files\Microsoft VS Code\Code.exe" -n --disable-extensions -d  "$LEFT" "$RIGHT"

# Return an errorcode of 0 if no differences were detected, 1 if some were.
# Any other errorcode will be treated as fatal.
```
### Change svn config file `$HOME/.subversion/config`

```
[helpers]
diff-cmd = $HOME/.subversion/external-diff-tool.sh
```
