# Docker

`/distribution/docker-compose.yml`

## Static
```yml
version : '3'
services:
  
  nginx:
    image: nginx
    port:
      - 1313:80
    volumes:
      - ../web:/usr/share/nginx/html
```

## Phalcon 
```yml
version: '3'

services:

  mongo:
    restart: always
    image: mongo:3.4
    expose:
      - 27017
    ports:
      - 27017:27017
    volumes:
      - /data/db:/data/db

  phalcon:
    restart: always
    image:  phalconphp/php-apache:ubuntu-16.04
    working_dir: /app
    ports:
      - 9000:9000
      - 80:80
    environment:
      - XDEBUG_ENABLE=1
      - XDEBUG_HOSTIP=10.0.75.1
      - XDEBUG_PORT=9000
    links:
      - mongo
    volumes:
      - ./..:/app

```