# Git

## Install

```bash
git config --global user.name "Jacques Pressigout"
git config --global user.email myemail@com

git config --global core.editor "vim"
```

## Config

```ini
[user]
	name = Jacques Pressigout
	email = myemail@com
	
[color]
  ui = auto
[color "branch"]
  upstream = cyan
  
[core]
  editor = 'c:/Program Files/Sublime Text 3/subl.exe' -w
  whitespace = -trailing-space
  autocrlf = false
  eol = lf
  
[log]
  abbrevCommit = true
  
[merge]
  conflictStyle = diff3
  
[push]
    default = simple
  
[pull]
    rebase = true
[rebase]
    autoStash = true
  
[rerere]
  enabled = true
  autoupdate = true
  
[alias]
  last = log -1 HEAD
  co = checkout
  st = status
  ci = commit
  lg = log --color --graph --pretty=format:'%C(red)%h%Creset -%C(yellow)%d%Creset %s %C(green)(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
  lga = log --color --graph --pretty=format:'%C(red)%h%Creset -%C(yellow)%d%Creset %s %C(green)(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --all
  oops = commit --amend --no-edit
```


## Tricks

### Commit on wrong branch

```bash
// Get commit N°
git lg

// Checkout right branch
git co -b prout origin/develop

// Cherry pick your commit
git cherry-pick N°

git push HEAD
```

### Normalize all files eol
```bash
git ls-files -z | xargs -0 rm
git checkout .
```

### Show repo url
```bash
git remote show origin
```

### Squash commits
```bash
git lga   #pick last develop commit id
git rebase -i $commit_id
```

### Rebase on another branch
```bash
git rebase --onto new_parent|commitID oldParent|commitID branch|commitID
```

### Force branch to commit
```bash
git branch -f branch_name commitID
```