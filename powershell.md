# PowerShell

## General 

```powershell

. "C:\Users\jpressigout\Documents\WindowsPowerShell\directory.ps1"
. "C:\Users\jpressigout\Documents\WindowsPowerShell\docker.ps1"
. "C:\Users\jpressigout\Documents\WindowsPowerShell\kubectl.ps1"

Set-Alias ip Get-NetIPAddress

function prompt
{
    Write-Host $ExecutionContext.SessionState.Path.CurrentLocation -ForegroundColor Green
    "$('>' * ($nestedPromptLevel + 1)) "
}

function vi
{
  param (
    $1
  )

  & 'C:\Program Files\Sublime Text 3\sublime_text.exe' $1
}

function vhosts {
  vi 'C:\Windows\System32\drivers\etc\hosts'
}

function cdproj  {
  cd C:\Projects\
}

function job(){
  $cmd = "Start-Job { $args }"
  Invoke-Expression $cmd
}

function clean-job(){
   Get-Job | Stop-Job
   Get-Job | Remove-Job
}


# Reset color after nasty leaving foreground color
$OrigBgColor = $host.ui.rawui.BackgroundColor
$OrigFgColor = $host.ui.rawui.ForegroundColor
function Reset-Colors {
    $host.ui.rawui.BackgroundColor = $OrigBgColor
    $host.ui.rawui.ForegroundColor = $OrigFgColor
}
```

## Directory

```powershell
Set-Alias ll dir

Set-Alias grep Select-String

function ggrep {
  param (
    $1,
    $2
  )

  findstr /s /n /c:"$1" $2
}

function find {
  param (
    $1
  )

  Get-ChildItem . -name -recurse *$1
}


Remove-Item Alias:cd
function cd {

  if ($args[0] -eq '-') {
    $pwd=$OLDPWD;
  } else {
    $pwd=$args[0];
  }

  $tmp=pwd;

  if ($pwd) {
    Set-Location $pwd;
  }

  Set-Variable -Name OLDPWD -Value $tmp -Scope global;
}


function rmdirforce {
  param (
    $1
  )

  Remove-Item -Recurse -Force $1
}

```

## Docker

```powershell
Set-Alias dc docker-compose

function d-bash {
  param (
    $1
  )

  docker exec -it $1 bash
}

function dc-logs {
  docker-compose logs -f --tail=1 $args
}

function d-deepclean {
    docker-compose rm
    docker rmi $(docker images -q)
    docker volume prune
}

```

## Git
```powershell
choco install poshgit

cd C:\tools\poshgit\dahlbyk-posh-git-a4faccd

Set-ExecutionPolicy RemoteSigned -Scope CurrentUser -Confirm
.\install.ps1
```

Add HOME evnironement variable to load user profile .gitconfig
```powershell
$env:HOME = $env:USERPROFILE
```

Then update the prompt as following 

```powershell
function prompt
{
    $origLastExitCode = $LASTEXITCODE
    Write-VcsStatus
    Write-Host $ExecutionContext.SessionState.Path.CurrentLocation -ForegroundColor Green
    $LASTEXITCODE = $origLastExitCode
    "$('>' * ($nestedPromptLevel + 1)) "
}

Import-Module 'C:\tools\poshgit\dahlbyk-posh-git-a4faccd\src\posh-git.psd1'

$global:GitPromptSettings.BeforeText = '['
$global:GitPromptSettings.AfterText  = '] '
```

### Update all repositories
```powershell
function git-co-all {
  param (
    $1
  )

  $dir = dir . | ?{$_.PSISContainer}

  $branch = If ($1) {$1} Else {"develop"}

  foreach ($d in $dir){
    cd $d.FullName
    echo "$pwd"
    git checkout $branch
    git fetch --prune
    git pull
  }
}
```

## Kubectl
```powershell
Set-Alias k kubectl

function k-bash {
  param (
    $1
  )

  kubectl exec -it $1 $args bash
}

function k-logs-pods {
  param (
    $1
  )

  $jobs = @()

  try{
    $pods = kubectl get po -o=custom-columns=NAME:.metadata.name,CONTAINERS:.spec.containers[*].name $args | grep $1
    $pods
    echo ""

    $logJobs,$logJobsCommand = getKubeLogsJobs $pods $args

    echo $logJobs.Length;

    ForEach($cmd in $logJobsCommand){
      echo "$cmd"
      $desc = Invoke-Expression $cmd
    }

    echo "logging ..."

    while ($true){
        ForEach($job in $logJobs){
            Receive-Job $job
        }
    }
  }
  finally {
    ForEach($job in $logJobs){
        Stop-Job $job
        Remove-Job $job
    }
  }
}

function getKubeLogsJobs(
    $pods,
    [string] $params
  )
{
  $logJobs = @()
  $logJobsCommand = @()

  ForEach($pod in $pods){
      $podName = "$pod".Split(" ")[0];
      $containerList = "$pod".Split(" ") | Select-Object -Last 1;

      if($containerList.Split(",").Length -gt 1){
        ForEach($container in $containerList.Split(",")){
          $jobName = "k-logs-$podName-$container"
          $logJobsCommand += "Start-Job -Name `"$jobName`" -ScriptBlock {kubectl logs po/$podName -c $container $params -f --tail=1}"
          $logJobs += $jobName
        }
      } else {
        $jobName = "k-logs-$podName"
        $logJobsCommand += "Start-Job -Name `"$jobName`" -ScriptBlock {kubectl logs po/$podName $params -f --tail=1}"
        $logJobs += $jobName
      }
  }

  return $logJobs,$logJobsCommand;
}
```